#!/usr/bin/env bash
function extract() {
    file_name=$1
    test_name=$2
    echo "$file_name $test_name"
    file_test=`echo $test_name  | sed 's/ /_/g'`
    tar -zxf "$file_name.jtl.tgz" -O | grep "$test_name" >  "$file_name.jtl"
    echo "compressing"
    tar -zcf "$file_name-$file_test.jtl.tgz" "$file_name.jtl"
    rm  "$file_name.jtl"
}
extract "jetty-clean-250thread" "Test API existing validation"
#extract "jetty-clean-250thread" "Test key existing validation"
#extract "jetty-clean-250thread" "Test CRUD"
#extract "jetty-clean-250thread" "Test Everything"